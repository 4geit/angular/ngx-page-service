import { TestBed, inject } from '@angular/core/testing';

import { NgxPageService } from './ngx-page.service';

describe('NgxPageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxPageService]
    });
  });

  it('should ...', inject([NgxPageService], (service: NgxPageService) => {
    expect(service).toBeTruthy();
  }));
});
