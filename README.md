# @4geit/ngx-page-service [![npm version](//badge.fury.io/js/@4geit%2Fngx-page-service.svg)](//badge.fury.io/js/@4geit%2Fngx-page-service)

---

Page-related routines

## Installation

1. A recommended way to install ***@4geit/ngx-page-service*** is through [npm](https://www.npmjs.com/search?q=@4geit/ngx-page-service) package manager using the following command:

```bash
npm i @4geit/ngx-page-service --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-page-service
```

2. You need to import the `NgxPageService` service in the module you want to use it. For instance `app.module.ts` as follows:

```js
import { NgxPageService } from '@4geit/ngx-page-service';
```

And you also need to add the `NgxPageService` service within the `@NgModule` decorator as part of the `providers` list:

```js
@NgModule({
  // ...
  providers: [
    // ...
    NgxPageService,
    // ...
  ],
  // ...
})
export class AppModule { }
```
